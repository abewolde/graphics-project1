function Planet(
    gl,
    shaderProgram,
    scale,
    dayPeriod,
    yearPeriod,
    distance,
    axisTilt,
    orbitTilt,
    name
) {
    var t = this;
    this.gl = gl;
    this.scale = scale;
    this.dayPeriod = dayPeriod;
    this.yearPeriod = yearPeriod;
    this.distance = distance;
    this.axisTilt = axisTilt;
    this.orbitTilt = orbitTilt;
    this.moons = [];
    this.name = name;

    this.vao = gl.createVertexArray();

    gl.bindVertexArray(this.vao);

    var positions = this.scalePlanet(this.scale);

    this.cubeBuffer = gl.createBuffer(); // get unique buffer ID number

    gl.bindBuffer(gl.ARRAY_BUFFER, this.cubeBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, positions, gl.STATIC_DRAW);

    // Specify locations of vertex coordinates in buffer for vPosition
    var floatBytes = 4; // number of bytes in a float value
    this.vPosition = gl.getAttribLocation(shaderProgram, "vPosition");
    gl.vertexAttribPointer(
        this.vPosition,
        3,
        gl.FLOAT,
        false,
        3 * floatBytes,
        0
    );
    gl.enableVertexAttribArray(this.vPosition);

    this.cubeColorBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, this.cubeColorBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, this.colorsArray, gl.STATIC_DRAW);
    this.vColor = gl.getAttribLocation(shaderProgram, "vColor");
    if (this.vColor === -1) {
        console.error("Attribute vPosition not found in shader program");
    }
    gl.vertexAttribPointer(this.vColor, 3, gl.FLOAT, false, 3 * floatBytes, 0);
    gl.enableVertexAttribArray(this.vColor);

    this.transformMat = gl.getUniformLocation(shaderProgram, "transformMat");

    var render = function () {
        t.Render();
    };

    gl.bindVertexArray(null);
}

Planet.prototype.Render = function (viewMat, location, change) {
    var gl = this.gl;
    // console.log(change);

    gl.bindVertexArray(this.vao);

    var modelMat = mat4();

    // console.log("one transformation cycled");
    // console.log(modelMat);

    if (this.orbitTilt > 0) {
        var orbit = rotateZ(this.orbitTilt);
        modelMat = mult(modelMat, orbit);
    }

    if (this.yearPeriod > 0) {
        var year = rotateY(this.yearPeriod * (change / 50));
        modelMat = mult(modelMat, year);
    }
    var t = translate(this.distance, 0, 0);
    modelMat = mult(modelMat, t);

    var tilt = rotateZ(this.axisTilt);
    modelMat = mult(modelMat, tilt);

    if (this.dayPeriod > 0) {
        var day = rotateX(this.dayPeriod + change);
        modelMat = mult(modelMat, day);
    }

    modelMat = mult(location, modelMat);
    var modelViewMat = mult(viewMat, modelMat);

    gl.uniformMatrix4fv(this.transformMat, false, flatten(modelViewMat));

    for (var start = 0; start < this.numVertices; start += this.faceVertices) {
        gl.drawArrays(gl.TRIANGLE_STRIP, start, this.faceVertices);
    }
    gl.bindVertexArray(null);
    for (var i = 0; i < this.moons.length; i++) {
        // console.log(
        //     "caller",
        //     this.name,
        //     "other moons",
        //     this.moons,
        //     "model mat",
        //     modelMat
        // );
        this.moons[i].Render(viewMat, modelMat, change);
    }
};

Planet.prototype.numVertices = 24; // total number of vertices
Planet.prototype.faceVertices = 4; // number of vertices in a face
Planet.prototype.positionsArray = Float32Array.of(
    // +x face
    0.25,
    -0.25,
    -0.25, // Vertex 1
    0.25,
    0.25,
    -0.25, // Vertex 2
    0.25,
    -0.25,
    0.25, // Vertex 3
    0.25,
    0.25,
    0.25, // Vertex 4

    // -x face
    -0.25,
    -0.25,
    0.25, // Vertex 1
    -0.25,
    0.25,
    0.25, // Vertex 2
    -0.25,
    -0.25,
    -0.25, // Vertex 3
    -0.25,
    0.25,
    -0.25, // Vertex 4

    // +y face
    -0.25,
    0.25,
    -0.25, // Vertex 1
    -0.25,
    0.25,
    0.25, // Vertex 2
    0.25,
    0.25,
    -0.25, // Vertex 3
    0.25,
    0.25,
    0.25, // Vertex 4

    // -y face
    0.25,
    -0.25,
    -0.25, // Vertex 1
    0.25,
    -0.25,
    0.25, // Vertex 2
    -0.25,
    -0.25,
    -0.25, // Vertex 3
    -0.25,
    -0.25,
    0.25, // Vertex 4

    // +z face
    -0.25,
    -0.25,
    0.25, // Vertex 1
    0.25,
    -0.25,
    0.25, // Vertex 2
    -0.25,
    0.25,
    0.25, // Vertex 3
    0.25,
    0.25,
    0.25, // Vertex 4

    // -z face
    -0.25,
    0.25,
    -0.25, // Vertex 1
    0.25,
    0.25,
    -0.25, // Vertex 2
    -0.25,
    -0.25,
    -0.25, // Vertex 3
    0.25,
    -0.25,
    -0.25 // Vertex 4
);

Planet.prototype.colorsArray = Float32Array.of(
    // +x face / cyan
    0.0,
    1.0,
    1.0, // Vertex 1
    0.0,
    1.0,
    1.0, // Vertex 2
    0.0,
    1.0,
    1.0, // Vertex 3
    0.0,
    1.0,
    1.0, // Vertex 4

    // -x face / red
    1.0,
    0.0,
    0.0, // Vertex 1
    1.0,
    0.0,
    0.0, // Vertex 2
    1.0,
    0.0,
    0.0, // Vertex 3
    1.0,
    0.0,
    0.0, // Vertex 4

    // +y face / magenta
    1.0,
    0.0,
    1.0, // Vertex 1
    1.0,
    0.0,
    1.0, // Vertex 2
    1.0,
    0.0,
    1.0, // Vertex 3
    1.0,
    0.0,
    1.0, // Vertex 4

    // -y face / green
    0.0,
    1.0,
    0.0, // Vertex 1
    0.0,
    1.0,
    0.0, // Vertex 2
    0.0,
    1.0,
    0.0, // Vertex 3
    0.0,
    1.0,
    0.0, // Vertex 4

    // +z face / yellow
    1.0,
    1.0,
    0.0, // Vertex 1
    1.0,
    1.0,
    0.0, // Vertex 2
    1.0,
    1.0,
    0.0, // Vertex 3
    1.0,
    1.0,
    0.0, // Vertex 4

    // -z face / blue
    0.0,
    0.0,
    1.0, // Vertex 1
    0.0,
    0.0,
    1.0, // Vertex 2
    0.0,
    0.0,
    1.0, // Vertex 3
    0.0,
    0.0,
    1.0 // Vertex 4
);

Planet.prototype.attachMoon = function (moon) {
    this.moons.push(moon);
};

Planet.prototype.scalePlanet = function (scale) {
    newPosition = [];
    for (var i = 0; i < this.positionsArray.length; i++) {
        newPosition.push(this.positionsArray[i] * scale);
    }
    var newPosition32 = new Float32Array(newPosition);
    return newPosition32;
};
