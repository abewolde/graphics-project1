/**
 * Lab 3 - COMP3801 Spring 2021
 *   ColorCube - draw a rotating cube with six different color faces
 */

"use strict";

/**
 * Constructor
 *
 * @param canvasID - string containing name of canvas to render.
 */
function Solar(canvasID) {
    var t = this; // save reference to this object for callbacks
    this.canvasID = canvasID;
    var canvas = (this.canvas = document.getElementById(canvasID));
    if (!canvas) {
        alert("Canvas ID '" + canvasID + "' not found.");
        return;
    }

    var gl = (this.gl = WebGLUtils.setupWebGL(this.canvas));
    if (!gl) {
        alert("WebGL isn't available in this browser");
        return;
    }

    // Find ratio of canvas width to height to correct stretching
    // that occurs when the viewport is not exactly square
    // (later this will be incorporated into the projection matrix).
    var aspectRatio = canvas.width / canvas.height;
    if (aspectRatio > 1.0) {
        this.aspectScale = scalem(1.0 / aspectRatio, 1.0, 1.0);
    } else {
        this.aspectScale = scalem(1.0, aspectRatio, 1.0);
    }

    // setting canvas color
    gl.clearColor(0, 0, 0, 1.0);

    // Enable hidden-surface removal (draw pixel closest to viewer)
    gl.enable(gl.DEPTH_TEST);

    // Compile and link shaders
    this.shaderProgram = initShaders(gl, "vShader.glsl", "fShader.glsl");
    if (this.shaderProgram === null) return;
    gl.useProgram(this.shaderProgram);

    // Set up callback to render a frame
    var render = function () {
        t.Render();
    };

    this.Sun = new Planet(gl, this.shaderProgram, 8, 0, 0, 0, 0, 0, "sun");
    this.Mercury = new Planet(
        gl,
        this.shaderProgram,
        1,
        10,
        100,
        5,
        0,
        7,
        "mercury"
    );
    this.Venus = new Planet(
        gl,
        this.shaderProgram,
        2,
        1,
        50,
        10,
        2,
        7,
        "venus"
    );
    this.Earth = new Planet(
        gl,
        this.shaderProgram,
        2,
        200,
        15,
        15,
        24,
        7,
        "earth"
    );
    this.EarthMoon = new Planet(
        gl,
        this.shaderProgram,
        0.5,
        100,
        100,
        2,
        7,
        80,
        "earthMoon"
    );
    this.Mars = new Planet(
        gl,
        this.shaderProgram,
        2,
        210,
        50,
        20,
        25,
        7,
        "mars"
    );
    this.Jupiter = new Planet(
        gl,
        this.shaderProgram,
        4,
        500,
        5,
        30,
        3,
        7,
        "jupiter"
    );
    this.JupiterEuropa = new Planet(
        gl,
        this.shaderProgram,
        0.5,
        100,
        100,
        2,
        0,
        7,
        "jupiterEuropa"
    );
    this.JupiterIo = new Planet(
        gl,
        this.shaderProgram,
        0.25,
        100,
        75,
        2.5,
        0,
        7,
        "jupiterIo"
    );
    this.JupiterGanymede = new Planet(
        gl,
        this.shaderProgram,
        0.75,
        100,
        125,
        3,
        0,
        7,
        "jupiterGanymede"
    );
    this.Saturn = new Planet(
        gl,
        this.shaderProgram,
        4,
        110,
        6,
        40,
        27,
        7,
        "saturn"
    );
    this.Uranus = new Planet(
        gl,
        this.shaderProgram,
        3,
        250,
        2,
        50,
        82,
        7,
        "uranus"
    );
    this.Neptune = new Planet(
        gl,
        this.shaderProgram,
        3,
        260,
        1,
        60,
        28,
        7,
        "neptune"
    );

    this.Sun.attachMoon(this.Mercury);
    this.Sun.attachMoon(this.Venus);
    this.Sun.attachMoon(this.Earth);
    this.Earth.attachMoon(this.EarthMoon);
    this.Sun.attachMoon(this.Mars);
    this.Jupiter.attachMoon(this.JupiterEuropa);
    this.Jupiter.attachMoon(this.JupiterIo);
    this.Jupiter.attachMoon(this.JupiterGanymede);
    this.Sun.attachMoon(this.Jupiter);
    this.Sun.attachMoon(this.Saturn);
    this.Sun.attachMoon(this.Uranus);
    this.Sun.attachMoon(this.Neptune);

    this.ship = new Ship(gl, this.shaderProgram);

    this.transformMat = gl.getUniformLocation(this.shaderProgram, "transformMat");

    this.change = 0;

    window.addEventListener("keydown", function (event) {
        t.handleKey(event);
    });

    var render = function () {
        t.Render();
    };
    var anim = function () {
        t.animate();
    };

    requestAnimationFrame(anim);
}

Solar.prototype.handleKey = function (event) {
    var code = event.keyCode;
    switch (code) {
        case 87: // w
            this.ship.moveUp(1);
            break;
        case 83: // s
            this.ship.moveUp(-1);
            break;
        case 65: // a
            this.ship.moveSideways(-1);
            break;
        case 68: // d
            this.ship.moveSideways(1);
            break;
        case 90: // z
            this.ship.moveForward(1);
            break;
        case 88: // x
            this.ship.moveForward(-1);
            break;
        case 81: // q
            this.ship.rotate(-1);
            break;
        case 69: // e
            this.ship.rotate(1);
            break;
    }
};

// Cube face coordinates (x,y,z), alternating with vertex colors (r,g,b)
Solar.prototype.cubeArray = Float32Array.of(
    0.25,
    -0.25,
    -0.25, // +x face / cyan
    0.0,
    1.0,
    1.0,
    0.25,
    0.25,
    -0.25,
    0.0,
    1.0,
    1.0,
    0.25,
    -0.25,
    0.25,
    0.0,
    1.0,
    1.0,
    0.25,
    0.25,
    0.25,
    0.0,
    1.0,
    1.0,

    -0.25,
    -0.25,
    0.25, // -x face / red
    1.0,
    0.0,
    0.0,
    -0.25,
    0.25,
    0.25,
    1.0,
    0.0,
    0.0,
    -0.25,
    -0.25,
    -0.25,
    1.0,
    0.0,
    0.0,
    -0.25,
    0.25,
    -0.25,
    1.0,
    0.0,
    0.0,

    -0.25,
    0.25,
    -0.25, // +y face / magenta
    1.0,
    0.0,
    1.0,
    -0.25,
    0.25,
    0.25,
    1.0,
    0.0,
    1.0,
    0.25,
    0.25,
    -0.25,
    1.0,
    0.0,
    1.0,
    0.25,
    0.25,
    0.25,
    1.0,
    0.0,
    1.0,

    0.25,
    -0.25,
    -0.25, // -y face / green
    0.0,
    1.0,
    0.0,
    0.25,
    -0.25,
    0.25,
    0.0,
    1.0,
    0.0,
    -0.25,
    -0.25,
    -0.25,
    0.0,
    1.0,
    0.0,
    -0.25,
    -0.25,
    0.25,
    0.0,
    1.0,
    0.0,

    -0.25,
    -0.25,
    0.25, // +z face / yellow
    1.0,
    1.0,
    0.0,
    0.25,
    -0.25,
    0.25,
    1.0,
    1.0,
    0.0,
    -0.25,
    0.25,
    0.25,
    1.0,
    1.0,
    0.0,
    0.25,
    0.25,
    0.25,
    1.0,
    1.0,
    0.0,

    -0.25,
    0.25,
    -0.25, // -z face / blue
    0.0,
    0.0,
    1.0,
    0.25,
    0.25,
    -0.25,
    0.0,
    0.0,
    1.0,
    -0.25,
    -0.25,
    -0.25,
    0.0,
    0.0,
    1.0,
    0.25,
    -0.25,
    -0.25,
    0.0,
    0.0,
    1.0
);
Solar.prototype.numVertices = 24; // total number of vertices
Solar.prototype.faceVertices = 4; // number of vertices in a face

/**
 * Render - draw the scene on the canvas
 *
 */
Solar.prototype.Render = function () {
    var gl = this.gl;
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // first viewport (spaceship view)
    gl.viewport(0, this.canvas.height / 2, this.canvas.width, this.canvas.height / 2);

    var aspect = this.canvas.width / (this.canvas.height / 2);
    var projectionMat1 = perspective(60, aspect, 0.1, 1000);
    var viewMat1 = this.ship.getViewMatrix();
    var cameraMatrix1 = mult(projectionMat1, viewMat1);

    this.Sun.Render(cameraMatrix1, mat4(), this.change);
    this.ship.Render(cameraMatrix1);

    // second viewport (top map view)
    gl.viewport(0, 0, this.canvas.width, this.canvas.height / 2);

    var zoomFactor = 2.0;
    var orthoAspect = this.canvas.width / (this.canvas.height / 2);
    var left = -50.0 * orthoAspect / zoomFactor;
    var right = 50.0 * orthoAspect / zoomFactor;
    var bottom = -50.0 / zoomFactor;
    var top = 50.0 / zoomFactor;
    var near = 0.1;
    var far = 500.0;

    var projectionMat2 = ortho(left, right, bottom, top, near, far);
    var viewMat2 = lookAt(vec3(0, 100, 0), vec3(0, 0, 0), vec3(0, 0, -1));
    var cameraMatrix2 = mult(projectionMat2, viewMat2);

    this.Sun.Render(cameraMatrix2, mat4(), this.change);
    this.ship.Render(cameraMatrix2);
};



Solar.prototype.animate = function () {
    this.change += 1;

    var render = () => this.Render();
    var anim = () => this.animate();
    requestAnimationFrame(render);
    requestAnimationFrame(anim);
};

Solar.prototype.getSliderValues = function () {
    var values = [];

    this.sliderIDs.map((id) => {
        var sliderID = this.canvasID + "-" + id + "-slider";
        var slider = document.getElementById(sliderID);
        values.push(slider.valueAsNumber);
    });

    return values;
};

function prettyPrintMat4(matrix) {
    let str = "[";
    for (let i = 0; i < matrix.length; i++) {
        for (let j = 0; j < matrix[0].length; j++) {
            str += " " + matrix[i][j].toString() + ",";
        }
        str += "\n";
    }
    str += "]";
    console.log(str);
}

function getCordinate(long, lat, rad) {
    var x =
        rad *
        Math.cos(lat * (Math.PI / 180)) *
        Math.cos(long * (Math.PI / 180));
    var y =
        rad *
        Math.cos(lat * (Math.PI / 180)) *
        Math.sin(long * (Math.PI / 180));
    var z = rad * Math.sin(lat * (Math.PI / 180));

    return [x, z, y];
}

function getDirectio(v, norm) {
    var res = [];
    for (var i = 0; i < v.length; i++) {
        if (norm[i] == 0) {
            res.push(0);
        } else {
            res.push(v[i] / norm[i]);
        }
    }
    return res;
}
