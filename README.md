

## Team Members
- Abenezer
- Ibraheem

## Issues Encountered
Everything seems to be working as expected. One minor issue is the keys are reversed for a couple of directions (w/s and q/e)

## Conclusion
Overall, the project successfully implements the intended camera movements, different viewports, and planets with their respective movements.