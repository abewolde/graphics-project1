function Ship(gl, shaderProgram) {
    var t = this;
    this.gl = gl;
    this.vao = gl.createVertexArray();

    gl.bindVertexArray(this.vao);

    this.cubeBuffer = gl.createBuffer(); // get unique buffer ID number

    gl.bindBuffer(gl.ARRAY_BUFFER, this.cubeBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, this.positionsArray, gl.STATIC_DRAW);

    var floatBytes = 4; // number of bytes in a float value
    this.vPosition = gl.getAttribLocation(shaderProgram, "vPosition");
    gl.vertexAttribPointer(
        this.vPosition,
        3,
        gl.FLOAT,
        false,
        3 * floatBytes,
        0
    );
    gl.enableVertexAttribArray(this.vPosition);

    this.cubeColorBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, this.cubeColorBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, this.colorsArray, gl.STATIC_DRAW);
    this.vColor = gl.getAttribLocation(shaderProgram, "vColor");
    if (this.vColor === -1) {
        console.error("Attribute vPosition not found in shader program");
    }
    gl.vertexAttribPointer(this.vColor, 3, gl.FLOAT, false, 3 * floatBytes, 0);
    gl.enableVertexAttribArray(this.vColor);

    this.transformMat = gl.getUniformLocation(shaderProgram, "transformMat");

    this.position = vec3(0, 0, 50);
    this.rotation = 0;

    var render = function () {
        t.Render();
    };

    gl.bindVertexArray(null);
}

Ship.prototype.moveForward = function (delta) {
    this.position[2] += delta * Math.cos(radians(this.rotation));
    this.position[0] += delta * Math.sin(radians(this.rotation));
};

Ship.prototype.moveSideways = function (delta) {
    this.position[2] += delta * Math.sin(radians(this.rotation));
    this.position[0] -= delta * Math.cos(radians(this.rotation));
};

Ship.prototype.moveUp = function (delta) {
    this.position[1] += delta;
};

Ship.prototype.rotate = function (theta) {
    this.rotation += theta;
};

Ship.prototype.getViewMatrix = function () {
    var translation = translate(-this.position[0], -this.position[1], -this.position[2]);
    var rotation = rotateY(-this.rotation);
    return mult(rotation, translation);
};

Ship.prototype.Render = function (viewMat) {
    var gl = this.gl;
    gl.bindVertexArray(this.vao);

    var modelMat = mat4();
    modelMat = mult(modelMat, translate(this.position[0], this.position[1], this.position[2]));
    modelMat = mult(modelMat, rotateY(this.rotation));

    var modelViewMat = mult(viewMat, modelMat);
    gl.uniformMatrix4fv(this.transformMat, false, flatten(modelViewMat));

    gl.drawArrays(gl.TRIANGLES, 0, 3);
    gl.bindVertexArray(null);
};

Ship.prototype.positionsArray = Float32Array.of(
    0.0,
    0.5,
    0.0, // Vertex 1
    -0.5,
    -0.5,
    0.0, // Vertex 2
    0.5,
    -0.5,
    0.0 // Vertex 3
);

// Define the color for each vertex (black)
Ship.prototype.colorsArray = Float32Array.of(
    1.0,
    1.0,
    1.0, // Vertex 1
    1.0,
    1.0,
    1.0, // Vertex 2
    1.0,
    1.0,
    1.0 // Vertex 3
);
